# Pour lancer le script :
# python classify.py --model animals.model --labelbin lb.pickle --image examples/animals/elephant_1.jpg

# importation des packages nécessaires
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import argparse
import imutils
import pickle
import cv2
import os

# On vérifie que tous les arguments nécessaires au bon fonctionnement du script
# ont bien été passés en entrée.
# model --> le modèle entrainé et qui a été sauvegardé lors du lancement du script train.py
# labelbin --> fichier contenant tous les labels des classes (chat / elephant etc.) et qui a été sauvegardé lors du lancement du script train.py
# image --> l'image à tester (NOTE : -- image examples/animals/elephant_1.jpg  classifie seulement l'image elephant_1.jpg alors
#							  que -- image no_image va classifier toutes les images du dossier /examples/animals.
#							  les résultats des classifications multiples sont situées dans le dossier examples/animals/results)
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", required=True,
	help="path to trained model model")
ap.add_argument("-l", "--labelbin", required=True,
	help="path to label binarizer")
ap.add_argument("-i", "--image", required=True,
	help="path to input image")
args = vars(ap.parse_args())



# images list
list_images = []						# pour stocker toutes les images
list_names = []							# pour stocker tous les noms d'image
list_labels = []						# pour stocker tous les labels d'image
list_indices_proba = []                 # pour stocker les indices des trois plus grosses probabilités
i = 0									# pour compter le nombre de fichiers dans "./examples/animals"
final_image_directory = "/results/"		# dossier d'écriture des résultats
final_image_directory_full_path = './examples/animals' + final_image_directory		# chemin relatif du dossier d'écriture des résultats

if not os.path.exists(final_image_directory_full_path):
 os.makedirs(final_image_directory_full_path)

# on supprime toutes les images du dossier de résultat
for filename in os.listdir(final_image_directory_full_path) :
    os.remove(final_image_directory_full_path + "/" + filename)

# vérification de l'argument -- image
if(args["image"] == "no_image"):

    # Pour toutes les images dans le dossier "animals"
    for element in os.listdir('./examples/animals'):
        if os.path.isdir("examples/animals/" + element):
            print("'%s' un dossier" % element)
        else:
            print("'%s' est un fichier" % element)

            element_renamed = "examples/animals/" + element

            # on charge l'image
            image = cv2.imread(element_renamed)
            list_images.append(image.copy())
            # output = image.copy()

            # formattage de l'image avant sa classification
            image = cv2.resize(image, (96, 96))
            image = image.astype("float") / 255.0
            image = img_to_array(image)
            image = np.expand_dims(image, axis=0)

			# chargement du modèle pré-entrainé
            model = load_model(args["model"])
            lb = pickle.loads(open(args["labelbin"], "rb").read())

            # classification de l'image d'entrée
            proba = model.predict(image)[0]
            # convert to list
            proba = proba.tolist()
            '''print("\n\nproba")
            print(proba)'''
            # on cherche les trois plus grosses probabilités et leurs indices
            proba_sorted = proba.copy()
            proba_sorted.sort(reverse=True)
            '''print("\n\nproba_sorted")
            print(proba_sorted)
            print("\n\nproba_sorted[0]")
            print(proba_sorted[0])
            print("\n\nproba.index(proba_sorted[0])")
            print(proba.index(proba_sorted[0]))'''
            list_indices_proba.append(proba.index(proba_sorted[0])) #   np.argmax(proba)
            list_indices_proba.append(proba.index(proba_sorted[1]))
            list_indices_proba.append(proba.index(proba_sorted[2]))
            label = lb.classes_[list_indices_proba[0]]
            label2 = lb.classes_[list_indices_proba[1]]
            label3 = lb.classes_[list_indices_proba[2]]
            '''print("\n\nlist_indices_proba")
            print(list_indices_proba)
            print("\n\nproba AGAIN")
            print(proba)'''

			# si le début du nom de l'image à classifier contient le bon label, alors
			# la prediction est marquée comme correcte
			# exemple :
			#			--> si la plus grande probabilité fait ressortir la classe chat et que l'image
			#				se nomme chat_myname.jpg, alors un correct en vert s'affichera sur l'image résultat
            filename = element_renamed[element_renamed.rfind(os.path.sep) + 1:]
            correct = "correct" if filename.rfind(label) != -1 else "incorrect"

            # construction du texte et fusion de ce dernier sur l'image à classifier
            label = "[1] : {}: {:.2f}% ({})".format(label, proba[list_indices_proba[0]] * 100, correct)
            label2 = "[2] : {}: {:.2f}%".format(label2, proba[list_indices_proba[1]] * 100)
            label3 = "[3] : {}: {:.2f}%".format(label3, proba[list_indices_proba[2]] * 100)

            list_images[i] = imutils.resize(list_images[i], width=400)
            if correct == "correct":
                # texte en vert si la prédiction est correcte
                cv2.putText(list_images[i], label, (10, 25),  cv2.FONT_HERSHEY_SIMPLEX,
                    0.7, (0, 255, 0), 2)
                cv2.putText(list_images[i], label2, (10, 50),  cv2.FONT_HERSHEY_SIMPLEX,
                    0.7, (0, 255, 0), 2)
                cv2.putText(list_images[i], label3, (10, 75),  cv2.FONT_HERSHEY_SIMPLEX,
                    0.7, (0, 255, 0), 2)
            else:
                # texte en rouge si la prédiction est incorrecte
                cv2.putText(list_images[i], label, (10, 25),  cv2.FONT_HERSHEY_SIMPLEX,
                    0.7, (0, 0, 255), 2)
                cv2.putText(list_images[i], label2, (10, 50),  cv2.FONT_HERSHEY_SIMPLEX,
                    0.7, (0, 0, 255), 2)
                cv2.putText(list_images[i], label3, (10, 75),  cv2.FONT_HERSHEY_SIMPLEX,
                    0.7, (0, 0, 255), 2)

            print(label)

            list_names.append(element)
            list_labels.append(label)

            list_indices_proba = [] # on clean la liste

            i = i + 1

else:

	# on charge l'image
    image = cv2.imread(args["image"])
    print(args["image"])
    output = image.copy()

    # formattage de l'image avant sa classification
    image = cv2.resize(image, (96, 96))
    image = image.astype("float") / 255.0
    image = img_to_array(image)
    image = np.expand_dims(image, axis=0)

    # chargement du modèle pré-entrainé
    print("[INFO] chargement du modele pre-entraine...")
    model = load_model(args["model"])
    lb = pickle.loads(open(args["labelbin"], "rb").read())

    # classification de l'image d'entrée
    print("[INFO] classification de l'image...")
    proba = model.predict(image)[0]
    idx = np.argmax(proba)
    label = lb.classes_[idx]

    # si le début du nom de l'image à classifier contient le bon label, alors
	# la prediction est marquée comme correcte
	# exemple :
	#			--> si la plus grande probabilité fait ressortir la classe chat et que l'image
	#				se nomme chat_myname.jpg, alors un correct en vert s'affichera sur l'image résultat
    filename = args["image"][args["image"].rfind(os.path.sep) + 1:]
    correct = "correct" if filename.rfind(label) != -1 else "incorrect"

    # construction du texte et fusion de ce dernier sur l'image à classifier
    label = "{}: {:.2f}% ({})".format(label, proba[idx] * 100, correct)
    output = imutils.resize(output, width=400)
    cv2.putText(output, label, (10, 25),  cv2.FONT_HERSHEY_SIMPLEX,
        0.7, (0, 255, 0), 2)

    # On affiche l'image à classifier avec le résultat de la classification
    print("[INFO] {}".format(label))
    cv2.imshow("Output", output)
    cv2.waitKey(0)


# pour le cas de multiples résultats, tous les résultats sont stockés dans le dossier "./examples/animals/results/"
u = 0
while u<(i):

    #cv2.imshow(list_names[u], list_images[u])
    cv2.imwrite('./examples/animals' + final_image_directory + "res_" +  list_names[u], list_images[u])
    u = u +1

if u!=0:
    # click pour continuer
    cv2.waitKey(0)