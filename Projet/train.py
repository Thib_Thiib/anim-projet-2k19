# Pour lancer le script
# python train.py --dataset dataset --model animals.model --labelbin lb.pickle

# pour l'utilisation de la carte graphique
import os
os.environ['CUDA_VISIBLE_DEVICES'] = "0"


# importation des packages nécessaires
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from keras.preprocessing.image import img_to_array
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from pyimagesearch.smallervggnet import SmallerVGGNet
import matplotlib.pyplot as plt
from imutils import paths
import numpy as np
import argparse
import random
import pickle
import cv2
import os
import matplotlib
matplotlib.use("Agg")

# On regarde si tous les arguments nécessaires en entrée du script ont bien été passees en params
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
	help="path to input dataset (i.e., directory of images)")
ap.add_argument("-m", "--model", required=True,
	help="path to output model")
ap.add_argument("-l", "--labelbin", required=True,
	help="path to output label binarizer")
ap.add_argument("-p", "--plot", type=str, default="plot.png",
	help="path to output accuracy/loss plot")
args = vars(ap.parse_args())

# nb epochs + niveau d'apprentissage initial + batch size + dimensions image
EPOCHS = 100
INIT_LR = 1e-3
BS = 32
IMAGE_DIMS = (96, 96, 3)

# initalisation data (tableau des images) + labels (debut du nom de l'image qui permet de reconnaitre la classe)
data = []
labels = []

print("[INFO] chargement des images...")
imagePaths = sorted(list(paths.list_images(args["dataset"])))
random.seed(42) # utilisation de la seed nécessaire pour pouvoir comparer les résultats d'une itération a une autre
random.shuffle(imagePaths) # melange des images

# Pour toutes les images
for imagePath in imagePaths:
	# load the image, pre-process it, and store it in the data list
	image = cv2.imread(imagePath) # charger l'image
	image = cv2.resize(image, (IMAGE_DIMS[1], IMAGE_DIMS[0])) # redimensionnement de l'image
	image = img_to_array(image) # stockage de l'image dans un tableau
	data.append(image) # ajout de l'image dans la liste d'images

	label = imagePath.split(os.path.sep)[-2]
	labels.append(label) # ajout du label dans la liste de labels

# On met à l'échellle les pixels (intervalle [0, 1])
data = np.array(data, dtype="float") / 255.0
labels = np.array(labels)
# affichage de la taille des données chargées
print("[INFO] matrice des donnees : {:.2f}MB".format(
	data.nbytes / (1024 * 1000.0)))

lb = LabelBinarizer()
labels = lb.fit_transform(labels)

# on split les datas en deux parties : 80% des images pour le training et 20% pour le test
(trainX, testX, trainY, testY) = train_test_split(data,
	labels, test_size=0.2, random_state=42)

# construct the image generator for data augmentation
# data augmentation pour rendre notre modèle plus robuste à l'overfitting
# on va creer de nouvelles images à partir des images déjà fournies
aug = ImageDataGenerator(rotation_range=25, width_shift_range=0.1,
	height_shift_range=0.1, shear_range=0.2, zoom_range=0.2,
	horizontal_flip=True, fill_mode="nearest")

# 					---- initalisation du modèle ----
# modèle : 	SmallerVGGNet --> CF fichier "smallervggnet.py" dans le dossier pyimagesearch
#		   	taille des images en entrée de ce modèle : 96*96*3
#		 	Nombre de classes : variable
print("[INFO] Compilation du modele...")
model = SmallerVGGNet.build(width=IMAGE_DIMS[1], height=IMAGE_DIMS[0],
	depth=IMAGE_DIMS[2], classes=len(lb.classes_))
opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)

# 					---- initalisation du modèle ----
# Loss function  : 	categorical_crossentropy
# Optimize		 :	Adam optimizer
# Metrics		 :  Liste des métriques à évaluer par le modèle (ici : Accuracy)
model.compile(loss="categorical_crossentropy", optimizer=opt,
	metrics=["accuracy"])

# 					---- Entrainement du réseau ----
# Loss function  			 : 	categorical_crossentropy
# donnees de validation		 :	testX et testY
# Verbose		 			 :  Precision des logs
print("[INFO] Entrainement Reseau en cours...")
H = model.fit_generator(
	aug.flow(trainX, trainY, batch_size=BS),
	validation_data=(testX, testY),
	steps_per_epoch=len(trainX) // BS,
	epochs=EPOCHS, verbose=2)

# on sauvegarde le modèle entrainé sur notre disque
print("[INFO] Sauvegarde du modele entraine...")
model.save(args["model"])

# On sauvegarde le label des classes classifiées afin de pouvoir tester
# notre modèle entrainé par la suite
print("[INFO] serializing label binarizer...")
f = open(args["labelbin"], "wb")
f.write(pickle.dumps(lb))
f.close()

# on trace la courbe représentant les différentes informations de notre modèle
# au cours de sa phase d'entrainement
plt.style.use("ggplot")
plt.figure()
N = EPOCHS
plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, N), H.history["accuracy"], label="train_acc")
plt.plot(np.arange(0, N), H.history["val_accuracy"], label="val_acc")
plt.title("Training Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="upper left")
plt.savefig(args["plot"])
plt.show()