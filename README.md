# Projet ANIM 2019/2020 : Classification et Reconnaissance d'images 

![RESULTS](https://image.noelshack.com/fichiers/2020/13/1/1584973115-results-anim.png)

# Introduction

Ce repository a pour but de stocker les fichiers du projet du cours "Image Analysis II"  
Le projet consiste en la mise d'un code de deep learning classifiant des images d'un même type puis permettant la reconnaissance de ces dernières.

# Arborescence des dossiers 

* *__Ressources__*  
	contient toutes les documentations et fichiers autre que du code  
	* *__Models__*  
		contient les fichiers de sauvegarde liés aux modèles entrainés et testés  
	* *__Autre__*  
		contient tous les autres fichiers
* *__Projet__*  
	contient tous les fichiers nécessaires pour utiliser notre projet  
	* *__dataset__*  
		Les images utilisées lors de la phase de training  
	* *__examples__*  
		Les images utilisées pour la classification  
	* *__pyimagesearch__*  
		Le modèle smallervggnet est stocké dans ce dossier  
	* *__classify.py__*  
		Le script permettant de classifier une ou plusieurs images  
	* *__train.py__*  
		Le script permettant d'entrainer notre modèle et de le sauvegarder  


# Installation

Liste des éléments à installer pour le bon fonctionnement de notre projet.

## Sous Windows

Installer pyzo-4.7.3 --> https://github.com/pyzo/pyzo/releases/tag/v4.7.3   (DL pyzo-4.7.3-win64.exe)  
Installer python 3.6.6 : https://www.python.org/downloads/release/python-366/  
Installer Python environment : Miniconda or Anaconda (Ouvrir pyzo et regarder ce qui est conseillé dans le cadre en haut à droite)  
Installer Keras : "pip install keras==2.2.5" dans le shell de Pyzo  
Installer Tensorflow : "pip install tensorflow==1.15" dans le shell de Pyzo

Ouvrir pyzo et clicker sur "use the environment" pour laisser pyzo configurer automatiquement l'IDE

--> pour faire fonctionner le code, de nombreux modules sont à installer comme sklearn , etc. Pour cela utiliser pip, exemple :  
	--> pip install imutils  
	--> pip install opencv-python  
	--> pip install sklearn

# Comment lancer les scripts classify.py et train.py ?

## classify.py

Pour lancer le script classify.py, il faut passer plusieurs paramètres en argument de ce script :

python classify.py --model animals.model --labelbin lb.pickle --image examples/animals/elephant_1.jpg

 model --> le modèle entrainé et qui a été sauvegardé lors du lancement du script train.py  
 labelbin --> fichier contenant tous les labels des classes (chat / elephant etc.) et qui a été sauvegardé lors du lancement du script train.py  
 image --> l'image à tester  
 __(NOTE : -- image examples/animals/elephant_1.jpg  classifie seulement l'image elephant_1.jpg alors__  
 __que -- image no_image va classifier toutes les images du dossier /examples/animals.__  
 __les résultats des classifications multiples sont situées dans le dossier examples/animals/results)__

## train.py

python train.py --dataset dataset --model animals.model --labelbin lb.pickle

 dataset --> le nom du dossier qui contient tous les dossiers d'images d'animaux
 labelbin --> fichier qui va contenir tous les labels des classes (chat / elephant etc.) et qui sera sauvegardé lors du lancement du script train.py  
 model --> le nom du modèle entrainé et qui a sera sauvegardé lors du lancement du script train.py

